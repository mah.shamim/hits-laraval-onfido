<?php

namespace Mahshamim\Onfido;

use Mahshamim\Onfido\Request;

class AddressPicker
{
    public $postcode;

    /**
     * @param null $postcode
     * @return mixed
     */
    public function pick($postcode = null)
    {
        $this->postcode = $postcode;
        $response = (new Request('GET', 'addresses/pick'))->send($this);
        return $response['addresses'];
    }

}
