<?php
namespace Mahshamim\Onfido;

use Mahshamim\Onfido\Config;

class Request
{

    private $method = 'GET';
    private $endpoint = '/';

    private $url = 'https://api.onfido.com/v';

    private $curlHandle;

    /**
     * Request constructor.
     * @param $method
     * @param $endpoint
     */
    public function __construct($method, $endpoint)
    {
        $this->method = $method;
        $this->endpoint = $endpoint;

        $this->curlHandle = curl_init();
    }

    /**
     * @param $params
     * @return mixed|string
     */
    public function send($params)
    {
        if (Config::init()->debug)
            var_dump(get_object_vars($params));

        $params = get_object_vars($params);

        $headers = Array('Authorization: Token token=' . Config::init()->token);

        $url_params = '';

        if ($this->method === 'PUT') {
            curl_setopt($this->curlHandle, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($this->curlHandle, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt($this->curlHandle, CURLOPT_VERBOSE, 1);
            curl_setopt($this->curlHandle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

            $this->prepare_params($params);

            curl_setopt($this->curlHandle, CURLOPT_POSTFIELDS, $params);
        }
        if ($this->method === 'POST') {

            curl_setopt($this->curlHandle, CURLOPT_POST, 1);
            $this->prepare_params($params);

            curl_setopt($this->curlHandle, CURLOPT_POSTFIELDS, $params);
        } else if ($this->method === 'GET') {
            $params['page'] = Config::init()->page;
            $params['per_page'] = Config::init()->per_page;

            $url_params = '?';

            $_url_params = Array();
            foreach ($params as $key => $value) {
                $_url_params[] = $key . '=' . urlencode($value);
            }

            $url_params .= implode('&', $_url_params);
        }


        curl_setopt($this->curlHandle, CURLOPT_HTTPHEADER,
            $headers
        );
        curl_setopt($this->curlHandle, CURLOPT_URL, $this->url . Config::init()->version . '/' . $this->endpoint . $url_params);

        curl_setopt($this->curlHandle, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($this->curlHandle);

        return $this->processResponse($response);

        curl_close($this->curlHandle);
    }

    /**
     * @param $params
     */
    private function prepare_params(&$params)
    {
        $output = is_array($params) ? Array() : (is_object($params) ? new \stdClass() : null);

        $file_upload = false;

        foreach (is_array($params) ? $params : (is_object($params) ? get_object_vars($params) : null) as $k => $v) {
            if ($k === 'file' && ($v instanceof \CurlFile || strpos($v, '@') === 0)) {
                $file_upload = true;
            }

            if ($k === 'id' || $k === 'created_at' || $v === null)
                continue;

            if (is_array($output))
                $output[$k] = $v;
            else if (is_object($output))
                $output->$k = $v;
        }
        if ($file_upload)
            $params = $output;
        else
            $params = preg_replace('/\[[0-9]+\]/', '[]', urldecode(http_build_query($output)));
    }

    /**
     * @param $response
     * @return mixed|string
     */
    private function processResponse($response)
    {
        if ($response === false)
            return 'cURL Error: ' . curl_error($this->curlHandle);

        $httpCode = curl_getinfo($this->curlHandle, CURLINFO_HTTP_CODE);
        try {
            $data = json_decode($response, true);
            $data['httpCode'] = $httpCode;

            return $data;
        } catch (Execption $e) {
            return $this->error("Couldn't parse the response, or general error happened !, Exception: " . json_encode($e));
        }
    }

    /**
     * @param $error
     * @return mixed
     */
    private function error($error)
    {
        return $error;
    }

}
