<?php

namespace Mahshamim\Onfido;

class Document
{
    public $id, $created_at, $href, $file_name, $file_type, $file_size, $type, $side, $file_path, $file, $applicant_id,
        $download_href, $issuing_country, $document_id;

    /**
     * @param $applicant_id
     * @return false|string
     */
    public function upload_for($applicant_id)
    {
        if (class_exists('\CurlFile'))
            $this->file = new \CurlFile($this->file_path, $this->file_type);
        else
            $this->file = '@' . $this->file_path;

        $response = (new Request('POST', 'documents/'))->send($this);
        return json_encode($response);
    }

    /**
     * @param $document_id
     * @return false|string
     */
    public function getDocumentList($applicant_id)
    {
        $this->applicant_id = $applicant_id;
        $response = (new Request('GET', 'documents'))->send($this);

        return json_encode($response);
    }

    /**
     * @param $document_id
     * @return false|string
     */
    public function getDocumentRetrieve($document_id)
    {
        $this->document_id = $document_id;
        $response = (new Request('GET', 'documents'))->send($this);

        return json_encode($response);
    }

    /**
     * @param $document_id
     * @return false|string
     */
    public function getDocumentDownload($document_id)
    {
        $response = (new Request('GET', 'documents/' . $document_id . '/download'))->send($this);

        return json_encode($response);
    }

}
