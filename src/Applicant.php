<?php

namespace Mahshamim\Onfido;

class Applicant
{
    public $id, $created_at, $href, $title, $first_name, $middle_name, $last_name, $email, $gender, $dob, $telephone,
        $mobile, $country, $id_numbers, $addresses, $applicant_id;

    /**
     * @return false|string
     */
    public function create()
    {
        $response = (new Request('POST', 'applicants'))->send($this);
        return json_encode($response);
    }

    /**
     * @param null $applicant_id
     * @return false|string
     */
    public function update($applicant_id = null)
    {
        $response = (new Request('PUT', 'applicants' . ($applicant_id !== null ? '/' . $applicant_id : '')))->send($this);
        return json_encode($response);
    }

    /**
     * @param null $applicant_id
     * @return false|string
     */
    public function get($applicant_id = null)
    {
        $this->applicant_id = $applicant_id;
        $response = (new Request('GET', 'applicants'))->send($this);

        if ($applicant_id !== null)
            return json_encode($response);
        else
            return $response->applicants;
    }

    /**
     * @param null $applicant_id
     * @return false|string
     */
    public function delete($applicant_id = null)
    {
        $response = (new Request('DELETE', 'applicants' . ($applicant_id !== null ? '/' . $applicant_id : '')))->send($this);
        return json_encode($response);
    }

    /**
     * @param null $applicant_id
     * @return false|string
     */
    public function restore($applicant_id = null)
    {
        $response = (new Request('POST', 'applicants' . ($applicant_id !== null ? '/' . $applicant_id : ''). '/restore'))->send($this);
        return json_encode($response);
    }

    /**
     * @param null $applicant_id
     * @return false|string
     */
    public function retrieve($applicant_id = null)
    {
        $this->applicant_id = $applicant_id;
        $response = (new Request('GET', 'applicants' . ($applicant_id !== null ? '/' . $applicant_id : '')))->send($this);

        if ($applicant_id !== null)
            return json_encode($response);
        else
            return $response->applicants;
    }

}
