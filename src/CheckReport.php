<?php
/**
 * Created by Carl Owens (carl@partfire.co.uk)
 * Company: PartFire Ltd (www.partfire.co.uk)
 * Copyright © 2016 PartFire Ltd. All rights reserved.
 *
 * User:    Carl Owens
 * Date:    09/11/2016
 * Time:    06:31
 * File:    CheckReport.php
 **/

namespace Mahshamim\Onfido;

class CheckReport
{
    public $name, $variant, $options, $applicant_id, $check_id, $report_names;

    /**
     * @param $document_id
     * @return false|string
     */
    public function getCheckList($applicant_id)
    {
        $this->applicant_id = $applicant_id;
        $response = (new Request('GET', 'checks'))->send($this);

        return json_encode($response);
    }

    /**
     * @param $check_id
     * @return false|string
     */
    public function getCheckRetrieve($check_id)
    {
        $this->check_id = $check_id;
        $response = (new Request('GET', 'checks'))->send($this);

        return json_encode($response);
    }

    /**
     * @param $check_id
     * @return false|string
     */
    public function getCheckResume($check_id)
    {
        $response = (new Request('POST', 'checks/' . $check_id . '/resume'))->send($this);

        return json_encode($response);
    }

}
