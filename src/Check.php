<?php

namespace Mahshamim\Onfido;

class Check
{
    public $id, $created_at, $href, $type, $status, $result, $reports, $report_names, $applicant_id;

    /**
     * @param $applicant_id
     * @return false|string
     */
    public function postCheckCreate($applicant_id)
    {
        $this->applicant_id = $applicant_id;
        $this->report_names = [
            "document",
            "facial_similarity_photo"
        ];
        $response = (new Request('POST', 'checks'))->send($this);

        return json_encode($response);
    }

}
