<?php
namespace Mahshamim\Onfido;

class Config
{
    public static $instance;
    public $token = '';
    public $version = '3';
    public $page = 1;
    public $per_page = 20;
    public $debug;

    /**
     * Config constructor.
     */
    protected function __construct()
    {

    }

    /**
     * @return mixed
     */
    public static function init()
    {
        if (static::$instance === null)
            static::$instance = new static();

        return static::$instance;
    }

    /**
     * @param $token
     * @return $this
     */
    public function set_token($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @param $version
     * @return $this
     */
    public function set_version($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @param null $page
     * @param null $per_page
     * @return $this
     */
    public function paginate($page = null, $per_page = null)
    {
        if ($page !== null)
            $this->page = $page;

        if ($per_page !== null)
            $this->per_page = $per_page;

        return $this;
    }

    /**
     * @return $this
     */
    public function debug()
    {
        $this->debug = true;

        return $this;
    }
}
