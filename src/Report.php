<?php

namespace Mahshamim\Onfido;

class Report
{
    public $id, $created_at, $href, $name, $status, $result, $breakdown, $properties, $check_id, $report_id;

    /**
     * @param $check_id
     * @return false|string
     */
    public function getReportList($check_id)
    {
        $this->check_id = $check_id;
        $response = (new Request('GET', 'reports'))->send($this);

        return json_encode($response);
    }

    /**
     * @param $report_id
     * @return false|string
     */
    public function getReportRetrieve($report_id)
    {
        $this->report_id = $report_id;
        $response = (new Request('GET', 'reports'))->send($this);

        return json_encode($response);
    }

    /**
     * @param $report_id
     * @return false|string
     */
    public function postReportCancel($report_id)
    {
        $this->report_id = $report_id;
        $response = (new Request('GET', 'reports/' . $report_id . '/cancel'))->send($this);

        return json_encode($response);
    }

    /**
     * @param $report_id
     * @return false|string
     */
    public function postReportResume($report_id)
    {
        $this->report_id = $report_id;
        $response = (new Request('GET', 'reports/' . $report_id . '/resume'))->send($this);

        return json_encode($response);
    }

}
